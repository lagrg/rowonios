//
//  ViewHRController.swift
//  Rowon
//
//  Created by Luis Garcia Ruiz on 2017-03-30.
//  Copyright © 2017 Luis Garcia Ruiz. All rights reserved.
//

import Foundation
import UIKit
import CoreBluetooth
import CoreMotion

//import FMDB

//internal let SQLITE_STATIC = unsafeBitCast(0, sqlite3_destructor_type.self)
class ViewHRController: UIViewController, CBCentralManagerDelegate, CBPeripheralDelegate {
    
    var centralManager:CBCentralManager!
    var connectingPeripheral:CBPeripheral!
    public var a = [0]
    public var sessionid = 1
    var start = false
    
  
    
    
    @IBOutlet var label1: UILabel!
    
    let HRSERVICE = "180D"
    let HRDEVICE = "180A"
    let fileURL = try! FileManager.default
        .url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        .appendingPathComponent("session.sqlite")
    
    
    
    
    
    override func viewDidLoad() {
        
       
        
        
        let heartRateServiceUUID = CBUUID(string: HRSERVICE)
        let deviceInfoServiceUUID = CBUUID(string: HRDEVICE)
        
        let services = [heartRateServiceUUID, deviceInfoServiceUUID];
        
        //let centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
        let centralManager = CBCentralManager(delegate: self, queue: nil)
        
        centralManager.scanForPeripherals(withServices: services, options: nil)
        
        //[centralManager scanForPeripheralsWithServices:services options:nil];
        self.centralManager = centralManager;
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        centralManager = CBCentralManager(delegate: self, queue: DispatchQueue.main)
    }
    
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        print("--- centralManagerDidUpdateState")
        switch central.state{
        case .poweredOn:
            print("poweredOn")
            
            let serviceUUIDs:[AnyObject] = [CBUUID(string: "180D")]
            let lastPeripherals = centralManager.retrieveConnectedPeripherals(withServices: serviceUUIDs as! [CBUUID])
            
            if lastPeripherals.count > 0{
                let device = lastPeripherals.last! as CBPeripheral;
                connectingPeripheral = device;
                centralManager.connect(connectingPeripheral, options: nil)
            }
            else {
                centralManager.scanForPeripherals(withServices: serviceUUIDs as? [CBUUID], options: nil)
                
            }
        case .poweredOff:
            print("off")
        case .resetting:
            print("resetting")
        case .unauthorized:
            print("No autorizado")
        case .unknown:
            print("cs unknown")
        case .unsupported:
            print("cs unsupported")
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        print("Nuevo!")
        
        if let localName = advertisementData[CBAdvertisementDataLocalNameKey] as? String{
            print("HRM: \(localName)")
            self.centralManager.stopScan()
            connectingPeripheral = peripheral
            connectingPeripheral.delegate = self
            centralManager.connect(connectingPeripheral, options: nil)
        }else{
            print("!!!data?!!!")
        }
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("Conectado")
        
        peripheral.delegate = self
        peripheral.discoverServices(nil)
        print("HRM estado: \(peripheral.state)")
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        if (error) != nil{
            print("error: \(error?.localizedDescription)")
        }
        else {
            print("error en didDiscoverServices")
            for service in peripheral.services as [CBService]!{
                peripheral.discoverCharacteristics(nil, for: service)
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        if (error) != nil{
            print("error en didDiscoverCharacteristicsFor: \(error?.localizedDescription)")
        }
        else {
            
            if service.uuid == CBUUID(string: "180D"){
                for characteristic in service.characteristics! as [CBCharacteristic]{
                    switch characteristic.uuid.uuidString{
                        
                    case "2A37":
                        // Set notification on heart rate measurement
                        print("Found a Heart Rate Measurement Characteristic")
                        peripheral.setNotifyValue(true, for: characteristic)
                        
                    case "2A38":
                        // Read body sensor location
                        print("Found a Body Sensor Location Characteristic")
                        peripheral.readValue(for: characteristic)
                        
                    case "2A29":
                        // Read body sensor location
                        print("Found a HRM manufacturer name Characteristic")
                        peripheral.readValue(for: characteristic)
                        
                    case "2A39":
                        // Write heart rate control point
                        print("Found a Heart Rate Control Point Characteristic")
                        
                        var rawArray:[UInt8] = [0x01];
                        let data = NSData(bytes: &rawArray, length: rawArray.count)
                        peripheral.writeValue(data as Data, for: characteristic, type: CBCharacteristicWriteType.withoutResponse)
                        
                    default:
                        print()
                    }
                    
                }
                
            }
            
        }
        
    }
    
    func update(heartRateData:Data){
        
        
        print("--- UPDATING ..")
        var buffer = [UInt8](repeating: 0x00, count: heartRateData.count)
        heartRateData.copyBytes(to: &buffer, count: buffer.count)
        
        // UIImage *image = [UIImage imageNamed:@"The-heart.png"];
        // UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        //  [UIView animateWithDuration:0.2f delay:3.0f options:0
        //  animations:^{imageView.alpha = 0.0;}
        //  completion:^{[imageView removeFromSuperview];}];
        
        
        
        var bpm:UInt16?
        if (buffer.count >= 2){
            if (buffer[0] & 0x01 == 0){
                bpm = UInt16(buffer[1]);
            }else {
                bpm = UInt16(buffer[1]) << 8
                bpm =  bpm! | UInt16(buffer[2])
            }
        }
        
        if let actualBpm = bpm{
            print(actualBpm)
            a+=[Int(actualBpm)]
            label1.text = ("\(actualBpm)")
            
            
            print(a)
        }else {
            label1.text = ("\(bpm!)")
            print(bpm!)
        }
        
        
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        print("didUpdateValueForCharacteristic")
        
        if (error) != nil{
            
        }else {
            switch characteristic.uuid.uuidString{
            case "2A37":
                update(heartRateData:characteristic.value!)
                
            default:
                print("diferente a  2A37 uuid charac")
            }
        }
    }
}

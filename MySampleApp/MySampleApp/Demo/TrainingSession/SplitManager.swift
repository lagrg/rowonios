//
//  SplitManager.swift
//  Rowonv01
//
//  Created by Luis Garcia Ruiz on 2017-04-12.
//  Copyright © 2017 Luis Garcia Ruiz. All rights reserved.
//
import CoreLocation

typealias Split = CLLocationSpeed
typealias Splittr = Int

protocol SplitManagerDelegate {
    func SplitDidChange(_ Splitm: Split,_ Splits:Split )
    func SplitDidChangetr<T>(_ Splitm: T,_ Splits:T )
}

class SplitManager: NSObject, CLLocationManagerDelegate {
    
    var delegate: SplitManagerDelegate?
    fileprivate let locationManager: CLLocationManager?
    
    override init() {
        locationManager = CLLocationManager.locationServicesEnabled() ? CLLocationManager() : nil
        
        super.init()
        
        if let locationManager = self.locationManager {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            
            if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.notDetermined {
                locationManager.requestAlwaysAuthorization()
            } else if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways {
                locationManager.startUpdatingLocation()
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedAlways {
            locationManager?.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if locations.count > 0 {
            let sp = 30.0 / (locations[locations.count - 1].speed * 3.6);
            let min = sp.rounded(.towardZero);
            var sec = 0;
            var minn = 0;
            
            if sp-min >= 0 && sp-min <= 1.1 {
                sec = Int(((sp-min) * 60.0 ).rounded());
                minn = Int(min);
            }
            
            if minn < 0 || minn > 99 {
                delegate?.SplitDidChangetr(0,0);
            }
            else{
                delegate?.SplitDidChangetr(minn,sec);
            }
        }
    }
}
